from fcm_django.models import FCMDevice

def push_share_notification(user, title, body, data):
    devices = FCMDevice.objects.filter(user=user)

    if devices:
        devices.send_message(
            title=title,
            body=body,
            data=data
        )