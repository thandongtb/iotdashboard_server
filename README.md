# PPowerTech IOT Platform

## Chức năng
Dự án server cho thiết bị thông minh bao gồm các chức năng:

* Điều khiển nhà thông minh
* Quản lý thiết bị, thêm mới, sửa đổi, theo dõi thiết bị
* Dễ dàng thêm mới các loại thiết bị
* Tạo Dashboard theo dõi dữ liệu gửi về từ thiết bị

## Cài đặt

### Clone this repo

Giả sử bạn clone repo này vào thư mục ``~/Project/iotdashboard`` chúng ta sẽ sử dụng nó cho các bước sau này.

### Môi trường phát triển
Dự án được viết trên nền tảng ngôn ngữ lập trình Python 3.5.2. Trước khi cài đặt bạn cần phải cài môi trường phát triển cho hệ thống bao gồm một số thành phần sau
* [VirtualENV](https://virtualenv.pypa.io/en/stable/) : sử dụng để chạy môi trường Python ảo trên máy local của bạn, độc lập với môi trường trên máy vật lý. Nó tương tự như một Docker Instance cho dự án Python
* Sau khi cài đặt VirtualENV xong bạn cần tạo một vỉttualenv trên máy của bạn:
```
virtualenv -p python3.5 ~/iot_server
```

* Tiếp sau đó chúng ta cần khởi động môi trường:
```
source ~/iot_server/bin/activate
```
* Cài đặt các gói phát triển:
```
cd ~/Project/iotdashboard
pip install -r requirements.txt
```

### Chạy migrate DB
```
python manage.py migrate --run-syncdb
```
### Tạo superuser

```
python manage.py createsuperuser
```

## Khởi chạy hệ thống:
```
python manage runserver [PORT]
```
trong đó **[PORT]** là cổng bạn muốn sử dụng để chạy server mặc định là cổng 8000

# Login Admin 

```bash
http://127.0.0.1:8000/en/admin/login/
```
Sau khi login vào lại trang [http://127.0.0.1:8000/](http://127.0.0.1:8000/) để xem kết quả
## Kết quả

![](https://sv1.uphinhnhanh.com/images/2018/04/09/test.png)

## Link API hiện có

[https://www.getpostman.com/collections/112142e25f494f1e5b19](https://www.getpostman.com/collections/112142e25f494f1e5b19)