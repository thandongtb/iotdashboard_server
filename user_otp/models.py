# -*- coding: utf-8 -*-
"""
User homes manager

"""

from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

@python_2_unicode_compatible
class UserOtp(models.Model):
    """
    User one time password manager.
    """
    user          = models.ForeignKey(User, on_delete=models.CASCADE, db_index=True)
    otp           = models.CharField(_('otp'), max_length=30, help_text="One Time Password")
    pub_date       = models.DateTimeField(_('pub_date'), auto_now=True)
    enable         = models.BooleanField(_('enable'), default=True)

