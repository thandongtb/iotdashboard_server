from django.apps import AppConfig


class UserOtpConfig(AppConfig):
    name = 'user_otp'
