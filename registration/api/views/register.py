from django.http import Http404
from rest_framework import serializers, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from user_otp.models import UserOtp
from rest_registration.decorators import (
    api_view_serializer_class,
    api_view_serializer_class_getter
)
from rest_registration.settings import registration_settings
from rest_registration.utils import (
    get_ok_response,
    get_user_by_id,
    get_user_setting,
    verify_signer_or_bad_request
)
import numpy as np
from rest_registration.verification import URLParamsSigner
from rest_framework_friendly_errors.mixins import FriendlyErrorMessagesMixin
from registration.send_mail import SendMail

class RegisterSigner(URLParamsSigner):
    salt = 'register'
    use_timestamp = True

    @property
    def base_url(self):
        return registration_settings.REGISTER_VERIFICATION_URL

    @property
    def valid_period(self):
        return registration_settings.REGISTER_VERIFICATION_PERIOD


@api_view_serializer_class_getter(
    lambda: registration_settings.REGISTER_SERIALIZER_CLASS)
@api_view(['POST'])
def register(request):
    '''
    Register new user.
    '''
    serializer_class = registration_settings.REGISTER_SERIALIZER_CLASS
    serializer = serializer_class(data=request.data)
    serializer.is_valid(raise_exception=True)

    kwargs = {}

    if registration_settings.REGISTER_VERIFICATION_ENABLED:
        verification_flag_field = get_user_setting('VERIFICATION_FLAG_FIELD')
        kwargs[verification_flag_field] = False

    user = serializer.save(**kwargs)

    output_serializer_class = registration_settings.REGISTER_OUTPUT_SERIALIZER_CLASS  # noqa: E501
    output_serializer = output_serializer_class(instance=user)
    user_data = output_serializer.data

    if registration_settings.REGISTER_VERIFICATION_ENABLED:
        userotp = UserOtp.objects.create(user=user, otp=np.random.randint(100000, 999999))

        SendMail(
            to_email=user_data['email'],
            subject="IK Home User Verification",
            body="Xin chân thành cảm ơn <br><br> <strong>IK Home Team</strong>",
            substitutions={
                '-name-' : user_data['first_name'] + ' ' + user_data['last_name'],
                '-code-' : userotp.otp
            }
        ).send()


    return Response({
        'code' : 1,
        'message' : 'Create user successful. Please check your email for verify this user.',
        'data' : user_data,
        'errors' : []
    })


class VerifyRegistrationSerializer(FriendlyErrorMessagesMixin, serializers.Serializer):
    user_id = serializers.CharField(required=True)
    timestamp = serializers.IntegerField(required=True)
    signature = serializers.CharField(required=True)


@api_view_serializer_class(VerifyRegistrationSerializer)
@api_view(['POST'])
def verify_registration(request):
    '''
    Verify registration via signature.
    '''
    process_verify_registration_data(request.data)
    return get_ok_response('User verified successfully')


def process_verify_registration_data(input_data):
    if not registration_settings.REGISTER_VERIFICATION_ENABLED:
        raise Http404()
    serializer = VerifyRegistrationSerializer(data=input_data)
    serializer.is_valid(raise_exception=True)

    data = serializer.validated_data
    signer = RegisterSigner(data)
    verify_signer_or_bad_request(signer)

    verification_flag_field = get_user_setting('VERIFICATION_FLAG_FIELD')
    user = get_user_by_id(data['user_id'], require_verified=False)
    setattr(user, verification_flag_field, True)
    user.save()
