from registration.utils import custom_validate_password
from rest_framework import permissions, serializers
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_registration.decorators import api_view_serializer_class
from iotdashboard.serializers import FriendlyErrorMessagesMixin

class ChangePasswordSerializer(FriendlyErrorMessagesMixin, serializers.Serializer):
    old_password = serializers.CharField()
    password = serializers.CharField()
    password_confirm = serializers.CharField()

    def validate_old_password(self, old_password):
        user = self.context['request'].user
        if not user.check_password(old_password):
            self.register_error(error_message='Old password is not correct',
                            error_code=8000,
                            field_name='old_password')
        return old_password

    def validate_password(self, password):
        user = self.context['request'].user
        custom_validate_password(password, user=user)
        return password

    def validate(self, data):
        if data['password'] != data['password_confirm']:
            self.register_error(error_message='Password confirm not match',
                                error_code=8000,
                                field_name='password_confirm')
        return data


@api_view_serializer_class(ChangePasswordSerializer)
@api_view(['POST'])
@permission_classes([permissions.IsAuthenticated])
def change_password(request):
    '''
    Change the user password.
    '''
    serializer = ChangePasswordSerializer(data=request.data,
                                          context={'request': request})
    serializer.is_valid(raise_exception=True)

    user = request.user
    user.set_password(serializer.validated_data['password'])
    user.save()

    return Response({
        'code': 1,
        'message': 'Password changed successfully',
        'data': [],
        'errors': []
    })