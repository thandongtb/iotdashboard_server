from rest_framework import serializers
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_registration.decorators import (
    api_view_serializer_class,
    api_view_serializer_class_getter
)
from rest_registration.settings import registration_settings
from rest_framework_friendly_errors.mixins import FriendlyErrorMessagesMixin
from django.conf import settings
import requests
from fcm_django.models import FCMDevice
from api.validators import ApiResponseValidators

@api_view_serializer_class_getter(
    lambda: registration_settings.LOGIN_SERIALIZER_CLASS)
@api_view(['POST'])
def login(request):
    '''
    Logs in the user via given username and password.
    '''
    serializer_class = registration_settings.LOGIN_SERIALIZER_CLASS
    serializer = serializer_class(data=request.data)
    serializer.is_valid(raise_exception=True)
    user = serializer.get_authenticated_user()

    if not user:
        return Response({
            'code': 4003,
            'message': 'Username or password is invalid',
            'data': [],
            'errors': []
        })
    else:
        token_data = get_token(user.username, request.data['password'])
        token_data['user_data'] = {
            'id' : user.pk,
            'username' : user.username,
            'email' : user.email,
            'is_active' : user.is_active,
            'pub_date' : user.date_joined
        }
        if 'access_token' in token_data:
            return Response({
                'code': 1,
                'message': 'Login successfully',
                'data' : token_data,
                'errors' : []
            })
        else:
            return Response({
                'code': 1,
                'message': 'User not active. Please verify this user',
                'data': token_data,
                'errors': []
            })

def get_token(username, password):
    post_data = {
        'client_id': settings.OAUTH2_CLIENT_ID,
        'client_secret': settings.OAUTH2_CLIENT_SECRET,
        'grant_type': 'password',
        'username': username,
        'password': password,
    }
    response = requests.post(settings.APP_URL + '/o/token/', data=post_data)
    return response.json()

def revoke_token(revoke_token):
    post_data = {
        'client_id': settings.OAUTH2_CLIENT_ID,
        'client_secret': settings.OAUTH2_CLIENT_SECRET,
        'token': revoke_token
    }
    requests.post(settings.APP_URL + '/o/revoke_token/', data=post_data)

class LogoutSerializer(FriendlyErrorMessagesMixin, serializers.Serializer):
    revoke_token = serializers.BooleanField(default=False)
    registration_id = serializers.CharField(required=True)


@api_view_serializer_class(LogoutSerializer)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def logout(request):
    '''
    Logs out the user. returns an error if the user is not
    authenticated.
    '''
    validator = ApiResponseValidators()
    serializer = LogoutSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    data = serializer.validated_data

    if validator.validate_access_token(request, kwargs=None) == True:
        return Response({
            'code': 1,
            'message': 'Invalid registration_id',
            'data': [],
            'errors': []
        })


    if data['revoke_token']:
        FCMDevice.objects.filter(registration_id=request.data['registration_id']).delete()
        revoke_token(request.auth)
        return Response({
            'code': 1,
            'message': 'Logout successfully and revoked your token',
            'data' : [],
            'errors' : []
        })

    return Response({
        'code': 1,
        'message': 'Logout successfully',
        'data': [],
        'errors': []
    })

