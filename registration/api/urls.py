from django.conf.urls import url

from .views import (
    change_password,
    login,
    logout,
    profile,
    register,
    verify
)

app_name = 'rest_registration'
urlpatterns = [
    url('^register/$', register, name='register'),
    url('^login/$', login, name='login'),
    url('^logout/$', logout, name='logout'),
    url('^profile/$', profile, name='profile'),
    url('^change-password/$', change_password, name='change-password'),
    url('^verify-email/$', verify, name='verify-email'),
]
