import sendgrid
from sendgrid.helpers.mail import Email, Content, Substitution, Mail
from iotdashboard.settings import ADMIN_EMAIL, VERIFY_USER_MAIL_TEMPLATE_ID, SENDGRID_API_KEY

class SendMail(object):
    def __init__(self, subject = "",  body="", to_email="", header="", template_id = VERIFY_USER_MAIL_TEMPLATE_ID, substitutions = None):
        self.from_mail = ADMIN_EMAIL
        self.header = header
        self.subject = subject
        self.body = body
        self.to_email = to_email
        self.template_id = template_id
        self.substitutions = substitutions
        self.api_key = SENDGRID_API_KEY

    def send(self):
        sg = sendgrid.SendGridAPIClient(apikey=self.api_key)
        from_email = Email(self.from_mail)
        subject = self.subject
        to_email = Email(self.to_email)
        content = Content("text/html", self.body)
        mail = Mail(from_email, subject, to_email, content)
        for k, v in self.substitutions.items():
            mail.personalizations[0].add_substitution(Substitution(k, v))
        mail.template_id = self.template_id
        try:
            response = sg.client.mail.send.post(request_body=mail.get())
        except:
            print('Error')
            pass
