from django.apps import AppConfig


class SceneRoomDevicesConfig(AppConfig):
    name = 'scene_room_devices'
