# -*- coding: utf-8 -*-
"""
Room device manager

"""

from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from room_devices.models import RoomDevices
from home_scenes.models import Scene
from django.utils.translation import ugettext_lazy as _

@python_2_unicode_compatible
class SceneRoomDevices(models.Model):
    """
    Scene device manager.
    Management the devices in the scenes they are using
    The device here equivalent with a channel in model.
    """
    scene          = models.ForeignKey(Scene, on_delete=models.CASCADE, db_index=True, related_name='scenes')
    room_device_id          = models.ForeignKey(RoomDevices, on_delete=models.CASCADE, db_index=True, related_name='scenes')
    command = models.IntegerField('command', default=0)
    pub_date       = models.DateTimeField(_('pub_date'), auto_now=True)
    enable         = models.BooleanField(_('enable'), default=True)

    class Meta:
        unique_together = (('scene', 'room_device_id'))

    def __str__(self):
        """
        """
        return self.device_name
