from django.apps import AppConfig


class UserRoomsConfig(AppConfig):
    name = 'home_rooms'
