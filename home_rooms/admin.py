from django.contrib import admin

# Register your models here.
from .models import Room

class RoomsAdmin(admin.ModelAdmin):
    list_display = ('home_id', 'name', 'enable')

admin.site.register(Room, RoomsAdmin)