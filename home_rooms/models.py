# -*- coding: utf-8 -*-
"""
Home rooms manager

"""

from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from user_homes.models import Home
from django.utils.translation import ugettext_lazy as _

@python_2_unicode_compatible
class Room(models.Model):
    """
    Home room manager.
    Management the rooms in the house of user
    """
    home_id          = models.ForeignKey(Home, on_delete=models.CASCADE, db_index=True, related_name='rooms')
    name           = models.CharField(_('room_name'), max_length=30, help_text="Room name created by user")
    pub_date       = models.DateTimeField(_('pub_date'), auto_now=True)
    enable         = models.BooleanField(_('enable'), default=True)
    type = models.BooleanField('type', default=True)

    class Meta:
        unique_together = ('home_id', 'name')

    def __str__(self):
        """
        """
        return self.name
