# -*- coding: utf-8 -*-
"""
URLs for project.

"""

from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.i18n import i18n_patterns
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve
from django.views.generic import RedirectView
from django.contrib.auth.models import User
from django.views.generic import TemplateView
import oauth2_provider.views as oauth2_views
from api.views.move_room_device import MoveRoomDeviceView
from rest_framework import routers, serializers, viewsets
from rest_framework.urlpatterns import format_suffix_patterns
from api.views.search_user import SearchUserView
from api.views.share_devices import ShareDevicePendingView, ShareDeviceConfirmView
from iotdashboard.settings import \
    IHOOK_WEB_SITE  # if ihook.xyz domain IHOOK_WEB_SITE = True, else IHOOK_WEB_SITE = False for local server

from panels import views as panels
from devices import views as devices
from channels import views as channels
from elements import views as elements
from datas import serializers as serializers_datas
from datas import views as views_datas
from api.views.command_iot import detect_command
from api.views.channels import DetailView as ChannelDetailView
router = routers.DefaultRouter()
# router.register(r'v1', datas.DataViewSet)
from rest_framework.documentation import include_docs_urls
from push_notification.fcm_django.api.rest_framework import FCMDeviceAuthorizedViewSet


if IHOOK_WEB_SITE:
    urlpatterns = i18n_patterns(
        # front page index page
        url(r'^$', TemplateView.as_view(template_name='front/index.html')),

        # backoffice panels index page
        url(r'^panel/$', panels.index, name='panels_index'),
    )
else:
    urlpatterns = i18n_patterns(
        # backoffice panels index page
        url(r'^$', include('landing_page.urls')),
        url(r'^panel/$', panels.index, name='panels_index'),
    )

urlpatterns += i18n_patterns(
    # add devices
    url(r'^device/add/$', devices.device_add, name='device_add'),
    url(r'^device/list/$', devices.device_list, name='device_list'),
    url(r'^device/edit/(?P<id>[^/]*)/$', devices.device_edit, name='device_edit'),
    url(r'^device/delete/(?P<id>[^/]*)/$', devices.device_delete, name='device_delete'),

    # add channel
    url(r'^channel/add/$', channels.channel_add, name='channel_add'),
    url(r'^channel/list/$', channels.channel_list, name='channel_list'),
    url(r'^channel/edit/(?P<id>[^/]*)/$', channels.channel_edit, name='channel_edit'),
    url(r'^channel/delete/(?P<id>[^/]*)/$', channels.channel_delete, name='channel_delete'),

    # channel api_key
    url(r'^key/list/$', channels.key_list, name='key_list'),
    url(r'^key/generate/(?P<id>[^/]*)/$', channels.generate_key, name='generate_key'),

    # add element
    url(r'^element/add/$', elements.element_add, name='element_add'),
    url(r'^element/list/$', elements.element_list, name='element_list'),
    url(r'^element/edit/(?P<id>[^/]*)/$', elements.element_edit, name='element_edit'),
    url(r'^element/delete/(?P<id>[^/]*)/$', elements.element_delete, name='element_delete'),

    # REST framework login to the Browsable API
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^docs/api/', include_docs_urls(title='PPowerTech API Documentations')),

    # data query
    url(r'^datas/$', views_datas.DataQueryList.as_view(), name='datas'),

    # chart
    url(r'^chart-view/(?P<id>[^/]*)/$', views_datas.chart_view, name='chart_view'),

    # realtime chart
    url(r'^chart-view/now/realtime/$', views_datas.chart_view_realtime, name='chart_view_realtime'),
    url(r'^chart-view/now/realtime/now/$', views_datas.chart_view_realtime_now, name='chart_view_realtime_now'),

    # export xls
    url(r'^export/(?P<model>[\w-]+)/$', views_datas.export, name='export'),

    # django admin page
    url(r'^admin/', admin.site.urls),

    # auth social
    url(r'^oauth/', include('social_django.urls', namespace='social')),
)

api_urlpatterns = [
    url('', include('api.urls')),
    url('accounts/', include('registration.api.urls')),
    url('detect-command/', detect_command, name='detect-command'),
    url(r'^fcm-devices?$', FCMDeviceAuthorizedViewSet.as_view({'post': 'create'}), name='create_fcm_device'),
]

urlpatterns += [
    url('api/v1/', include(api_urlpatterns)),

    url(r'^api/v1/channel-detail/(?P<channel_api_key>[^/]*)$',
         ChannelDetailView.as_view(), name='channel-detail'),
    url(r'^api/v1/home/(?P<home_id_pk>[^/]*)/move_room_device/(?P<pk>\d+)/to_new_room/(?P<room_id_pk>[^/]*)/$', MoveRoomDeviceView.as_view(), name='book_update'),

    url(r'^api/v1/search-user/', SearchUserView.as_view(), name='search-user'),
    url(r'^api/v1/share-pendding/', ShareDevicePendingView.as_view(), name='share-pending'),
    url(r'^api/v1/share-confirm/(?P<token>[^/]*)$', ShareDeviceConfirmView.as_view(), name='share-comfirm'),
]

urlpatterns += [
    # favicon
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico', permanent=True)),

    # REST framework
    url('o/', include('oauth2_provider.urls', namespace="oauth2_provider")),
    url(r'^api/v1/datas/(?P<api_key>[^/]*)/?$', views_datas.DataList.as_view()),
    url(r'^api/v1/datas/(?P<pk>[0-9]+)/(?P<api_key>[^/]*)/?$',  views_datas.DataDetail.as_view()),
]

if settings.DEBUG == True:
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT, }),
    ]

if settings.DEBUG == False:
    urlpatterns += [
        url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT, }),
    ]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urlpatterns = format_suffix_patterns(urlpatterns)
