import paho.mqtt.client as mqtt
from django.utils import timezone
from influxdb import InfluxDBClient
import iotdashboard.settings as settings

def on_connect(client, userdata,flags, rc):
    if rc == 0:
        print("connected OK Returned code=", rc)
    else:
        print("Bad connection Returned code=", rc)
    client.subscribe("{}/#".format(settings.MQTT_PREFIX))

def on_message(client, userdata, msg):
    # Do something
    receiveTime = timezone.localtime(timezone.now())
    message = msg.payload.decode("utf-8")
    payload = message.split('/')
    msgs = msg.topic.split('/')
    if len(payload) == 2 and len(msgs) == 4:
        user_id = msgs[1]
        home_id = msgs[2]
        mac_address = msgs[3]
        value = payload[1]
        try:
            value = float(value)
            isfloatValue = True
        except ValueError:
            isfloatValue = False
            pass

        if isfloatValue:
            json_body = [
                {
                    "measurement": 'smart_seed_button',
                    "time": receiveTime,
                    "fields": {
                        "value": value,
                        'user_id' : user_id,
                        'home_id' : home_id,
                        'mac_address' : mac_address
                    }
                }
            ]

            dbclient.write_points(json_body)

dbclient = InfluxDBClient(settings.INFLUXDB_SERVER, settings.INFLUXDB_PORT, settings.INFLUXDB_USERNAME, settings.INFLUXDB_PASSWORD, settings.INFLUXDB_DATABASE)

client = mqtt.Client()
client.username_pw_set(settings.MQTT_USERNAME, settings.MQTT_PASSWORD)
client.on_connect = on_connect
client.on_message = on_message

client.connect(settings.MQTT_SERVER, settings.MQTT_PORT, 60)