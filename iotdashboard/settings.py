# -*- coding: utf-8 -*-
"""
Settings for project.

"""

import os
import datetime
from django.utils.translation import ugettext_lazy as _

import dotenv

dotenv.load('../.env')


def env(key, default=None):
    value = dotenv.get(key, default)
    if value == 'True':
        value = True
    elif value == 'False':
        value = False
    return value


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

APP_URL = env('APP_URL')

ADMINS = (
    (u'admin', 'admin@ppowertech.vn'),
)

SECRET_KEY = env('APP_KEY')

WEBSITE_NAME = "IK Home"

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'rest_framework',
    'rest_framework_docs',
    'markup_deprecated',
    'oauth2_provider',
    'corsheaders',
    'django_extensions',
    'chartit',
    'panels',
    'devices',
    'channels',
    'elements',
    'datas',
    'drawcharts',
    'underthesea',
    'room_devices',
    'rest_registration',
    'api',
    'user_homes',
    'home_scenes',
    'scene_room_devices',
    'home_rooms',
    'landing_page.apps.LandingPageConfig',
    'social_django',
    'share_devices',
    'django_filters',
    'user_otp',
    'fcm_django',
]

FCM_DJANGO_SETTINGS = {
    "FCM_SERVER_KEY": env('FCM_API_KEY'),
     # true if you want to have only one active device per registered user at a time
     # default: False
    "ONE_DEVICE_PER_USER": False,
     # devices to which notifications cannot be sent,
     # are deleted upon receiving error response from FCM
     # default: False
    "DELETE_INACTIVE_DEVICES": False,
}

EMAIL_BACKEND = "sgbackend.SendGridBackend"

SENDGRID_API_KEY = env('SENDGRID_API_KEY')

ADMIN_EMAIL = env('ADMIN_EMAIL')

VERIFY_USER_MAIL_TEMPLATE_ID = env('VERIFY_USER_MAIL_TEMPLATE_ID')

REST_REGISTRATION = {
    'USER_LOGIN_FIELDS': ['username', 'email'],
    'USER_HIDDEN_FIELDS': (
        'is_active',
        'is_staff',
        'is_superuser',
        'user_permissions',
        'groups',
        'date_joined',
    ),
    'USER_PUBLIC_FIELDS': None,
    'USER_EMAIL_FIELD': 'email',

    'USER_VERIFICATION_FLAG_FIELD': 'is_active',

    'REGISTER_SERIALIZER_CLASS': 'registration.api.serializers.DefaultRegisterUserSerializer',
    'REGISTER_SERIALIZER_PASSWORD_CONFIRM': True,

    'REGISTER_VERIFICATION_ENABLED': True,
    'REGISTER_VERIFICATION_PERIOD': datetime.timedelta(days=7),
    'REGISTER_VERIFICATION_URL': 'api/v1/verify_user',
    'REGISTER_VERIFICATION_EMAIL_TEMPLATES': {
        'subject': 'rest_registration/register/subject.txt',
        'body': 'rest_registration/register/body.txt',
    },

    'LOGIN_SERIALIZER_CLASS': 'registration.api.serializers.DefaultLoginSerializer',
    'LOGIN_AUTHENTICATE_SESSION': None,
    'LOGIN_RETRIEVE_TOKEN': None,

    'RESET_PASSWORD_VERIFICATION_PERIOD': datetime.timedelta(days=1),
    'RESET_PASSWORD_VERIFICATION_URL': '/reset-password',
    'RESET_PASSWORD_VERIFICATION_EMAIL_TEMPLATES': {
        'subject': 'rest_registration/reset_password/subject.txt',
        'body': 'rest_registration/reset_password/body.txt',
    },

    'REGISTER_EMAIL_VERIFICATION_ENABLED': False,
    'REGISTER_EMAIL_VERIFICATION_PERIOD': datetime.timedelta(days=7),
    'REGISTER_EMAIL_VERIFICATION_URL': None,
    'REGISTER_EMAIL_VERIFICATION_EMAIL_TEMPLATES': {
        'subject': 'rest_registration/register_email/subject.txt',
        'body': 'rest_registration/register_email/body.txt',
    },

    'PROFILE_SERIALIZER_CLASS': 'registration.api.serializers.DefaultUserProfileSerializer',

    'VERIFICATION_FROM_EMAIL': 'no-reply@ppowertech.vn',
    'VERIFICATION_REPLY_TO_EMAIL': 'toanpv@ppowertech.vn',

    'SUCCESS_RESPONSE_BUILDER': 'registration.utils.build_default_success_response',
}

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'oauth2_provider.middleware.OAuth2TokenMiddleware',
    'social_django.middleware.SocialAuthExceptionMiddleware',
]

ROOT_URLCONF = 'iotdashboard.urls'

CORS_ORIGIN_ALLOW_ALL = True

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                # auth social
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
        },
    },
]

WSGI_APPLICATION = 'iotdashboard.wsgi.application'

HEROKU = False  # if heroku server HEROKU = True,  else HEROKU = False for local server
IHOOK_WEB_SITE = False  # if ihook.xyz domain IHOOK_WEB_SITE = True, else IHOOK_WEB_SITE = False for local server

DEBUG = True

if HEROKU:
    import dj_database_url

    DATABASES = {}
    DATABASES['default'] = dj_database_url.config()
    DATABASES['default']['CONN_MAX_AGE'] = 500

    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

    MEDIA_ROOT = os.path.join(BASE_DIR, 'iotdashboard_media')
    STATIC_ROOT = os.path.join(BASE_DIR, "iotdashboard_static")
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'iotdashboard_db.sqlite3'),
        }
    }

    # DATABASES = {
    #     'default': {
    #         'ENGINE': 'django.db.backends.postgresql_psycopg2',
    #         'NAME': env('DB_DATABASE'),
    #         'USER': env('DB_USERNAME'),
    #         'PASSWORD': env('DB_PASSWORD'),
    #         'HOST': env('DB_HOST'),
    #         'PORT': env('DB_PORT'),
    #     }
    # }

    MEDIA_ROOT = os.path.join(BASE_DIR, '../iotdashboard_media/')
    STATIC_ROOT = os.path.join(BASE_DIR, "../iotdashboard_static/")

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

_ = lambda s: s

LANGUAGES = [
    ('vi', _('Vietnam')),
    ('en', _('English')),
]

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

LANGUAGE_CODE = 'en'

TIME_ZONE = 'Asia/Ho_Chi_Minh'

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_URL = '/media/'
STATIC_URL = '/static/'

MQTT_PREFIX=env('MQTT_PREFIX')

MQTT_SERVER=env('MQTT_SERVER')
MQTT_PORT=env('MQTT_PORT')
MQTT_USERNAME=env('MQTT_USERNAME')
MQTT_PASSWORD=env('MQTT_PASSWORD')

INFLUXDB_SERVER=env('INFLUXDB_SERVER')
INFLUXDB_PORT=env('INFLUXDB_PORT')
INFLUXDB_DATABASE=env('INFLUXDB_DATABASE')
INFLUXDB_USERNAME=env('INFLUXDB_USERNAME')
INFLUXDB_PASSWORD=env('INFLUXDB_PASSWORD')

STATICFILES_DIRS = (
    BASE_DIR + '/static/',
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

LOGIN_URL = '/admin/login/'
LOGIN_REDIRECT_URL = '/'
LOGIN_ERROR_URL = '/'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

AUTHENTICATION_BACKENDS = [
    'social_core.backends.twitter.TwitterOAuth',
    'social_core.backends.facebook.FacebookOAuth2',
    'social_core.backends.google.GooglePlusAuth',
    'social_core.backends.instagram.InstagramOAuth2',
    'oauth2_provider.backends.OAuth2Backend',
    'registration.user_backend.EmailOrUsernameModelBackend',
]

OAUTH2_PROVIDER = {
    'ACCESS_TOKEN_EXPIRE_SECONDS': 30*24*60*60,
    'REFRESH_TOKEN_EXPIRE_SECONDS': None,
    'ALLOWED_REDIRECT_URI_SCHEMES': ["https", "http"],
    'CLIENT_SECRET_GENERATOR_LENGTH': 255,
    'DEFAULT_SCOPES': [],
    'SCOPES': {
        'read': 'Read scope',
        'write': 'Write scope'
    }
}

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
    ],

    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.AllowAny',
    ],

    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',),

    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.NamespaceVersioning',
    'PAGINATE_BY': 10,
    'EXCEPTION_HANDLER' : 'iotdashboard.handlers.friendly_exception_handler'
}

REST_FRAMEWORK_DOCS = {
    'HIDE_DOCS': False  # Default: False
}

USERNAME_MODE = env('USERNAME_MODE')
SOCIAL_AUTH_REDIRECT_IS_HTTPS = True
# auth social config
# Twitter
SOCIAL_AUTH_TWITTER_KEY = env('SOCIAL_AUTH_TWITTER_KEY')
SOCIAL_AUTH_TWITTER_SECRET = env('SOCIAL_AUTH_TWITTER_SECRET')
# facebook
SOCIAL_AUTH_FACEBOOK_KEY = env('SOCIAL_AUTH_FACEBOOK_KEY')
SOCIAL_AUTH_FACEBOOK_SECRET = env('SOCIAL_AUTH_FACEBOOK_SECRET')
# Instagram
SOCIAL_AUTH_INSTAGRAM_KEY = env('SOCIAL_AUTH_INSTAGRAM_KEY')
SOCIAL_AUTH_INSTAGRAM_SECRET = env('SOCIAL_AUTH_INSTAGRAM_SECRET')
# Google
SOCIAL_AUTH_GOOGLE_PLUS_KEY = env('SOCIAL_AUTH_GOOGLE_PLUS_KEY')
SOCIAL_AUTH_GOOGLE_PLUS_SECRET = env('SOCIAL_AUTH_GOOGLE_PLUS_SECRET')

OAUTH2_CLIENT_ID = env('OAUTH2_CLIENT_ID')
OAUTH2_CLIENT_SECRET = env('OAUTH2_CLIENT_SECRET')
