from django.apps import AppConfig


class UsersSceneConfig(AppConfig):
    name = 'users_scene'
