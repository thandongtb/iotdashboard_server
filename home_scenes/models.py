# -*- coding: utf-8 -*-
"""
User homes manager

"""

from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.contrib.auth.models import User
from user_homes.models import Home
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField

@python_2_unicode_compatible
class Scene(models.Model):
    """
    User scene manager.
    Management the user and houses they are using
    """
    home_id = models.ForeignKey(Home, on_delete=models.CASCADE, db_index=True, related_name='scenes')
    name           = models.CharField(_('scene_name'), max_length=30, help_text="Scene name created by user")
    pub_date       = models.DateTimeField(_('pub_date'), auto_now=True)
    enable         = models.BooleanField(_('enable'), default=True)

    def __str__(self):
        """
        """
        return self.name
