# -*- coding: utf-8 -*-
"""
Datas

"""

from __future__ import unicode_literals
from django.db import models
from django.template.defaultfilters import slugify as djslugify
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
import hashlib, random

from channels.models import Channel

class Data(models.Model):
    """
    Requests for iot device
    """
    owner          = models.ForeignKey(User, related_name='datas', on_delete=models.CASCADE,)
    remote_address = models.CharField(_('ip_address'), max_length=255)
    channel        = models.ForeignKey(Channel, on_delete=models.CASCADE,)
    name_id        = models.CharField(_('name_id'), max_length=70, null=False, blank=False)
    value          = models.CharField(_('value'), max_length=10, null=False, blank=False)
    pub_date       = models.DateTimeField(_('pub_date'), auto_now=True)

    def __unicode__(self):
        return unicode(self.channel.name)

