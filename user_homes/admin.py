from django.contrib import admin

# Register your models here.
from .models import Home

class HomeAdmin(admin.ModelAdmin):
    list_display = ('owner', 'name','pub_date', 'enable')

admin.site.register(Home, HomeAdmin)