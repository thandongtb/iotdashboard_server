# -*- coding: utf-8 -*-
"""
User homes manager

"""

from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.contrib.auth.models import User
from devices.models import Device
from django.utils.translation import ugettext_lazy as _

@python_2_unicode_compatible
class Home(models.Model):
    """
    User home manager.
    Management the user and houses they are using
    """
    owner          = models.ForeignKey(User, on_delete=models.CASCADE, db_index=True)
    name           = models.CharField(_('home_name'), max_length=30, help_text="Home name created by user")
    pub_date       = models.DateTimeField(_('pub_date'), auto_now=True)
    enable         = models.BooleanField(_('enable'), default=True)
    is_favorite = models.BooleanField('is_favorite', default=False)
    def __str__(self):
        """
        """
        return self.name
