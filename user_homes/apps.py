from django.apps import AppConfig


class UserHomesConfig(AppConfig):
    name = 'user_homes'
