from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from room_devices.models import RoomDevices
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from user_homes.models import Home
from datetime import datetime, timedelta
from django.utils import timezone

@python_2_unicode_compatible
class ShareRoomDevices(models.Model):
    """
    Share device manager.
    """
    origin_user          = models.ForeignKey(User, on_delete=models.CASCADE, db_index=True, related_name='share')
    shared_user          = models.ForeignKey(User, on_delete=models.CASCADE, db_index=True)
    home          = models.ForeignKey(Home, on_delete=models.CASCADE, db_index=True, related_name='share')
    token = models.TextField('token', blank=True)
    expired_at = models.DateTimeField('expired_at', default=datetime.now()+timedelta(days=7))
    pub_date       = models.DateTimeField(_('pub_date'), default=datetime.now())
    enable         = models.BooleanField(_('enable'), default=True)

    class Meta:
        unique_together = (('shared_user', 'home'))
