from django.apps import AppConfig


class ShareDevicesConfig(AppConfig):
    name = 'share_devices'
