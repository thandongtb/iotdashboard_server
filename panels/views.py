# -*- coding: utf-8 -*-
"""
Panels page

"""

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login

def index(request):
    """
    :param request:
    :return:
    """
    # auto login for test users
    if request.user.is_superuser:
        return render(request, "back/index.html", locals())
    return redirect("/admin/login")  # or your url name


