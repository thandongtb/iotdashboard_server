from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from home_rooms.models import Room
from room_devices.models import RoomDevices
from scene_room_devices.models import SceneRoomDevices
from user_homes.models import Home
from devices.models import Device
from channels.models import Channel
from elements.models import Element
from iotdashboard.serializers import FriendlyErrorMessagesMixin
from home_scenes.models import Scene
from django.db import transaction
from django.contrib.auth.models import User
from share_devices.models import ShareRoomDevices

class UserSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')


class DeviceSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    owner_id = serializers.IntegerField(source='owner.id', read_only=True)

    class Meta:
        model = Device
        fields = ('id', 'owner_id', 'name', 'slug', 'image', 'pub_date', 'description', 'enable')
        read_only_fields = ('id', 'owner_id', 'slug', 'pub_date')


class HomeSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    owner_id = serializers.IntegerField(source='owner.id', read_only=True)

    @transaction.atomic
    def create(self, validated_data):
        home = Home.objects.create(**validated_data)
        room_data = {
            'name' : 'Default Room',
            'home_id' : home,
            'enable' : 1,
            'type' : 0
        }
        Room.objects.create(**room_data)
        return home

    class Meta:
        model = Home
        fields = ('id', 'owner_id', 'name', 'pub_date', 'enable', 'is_favorite')
        read_only_fields = ('id', 'owner_id')


class RoomSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    home_id = serializers.IntegerField(source='home.id', read_only=True)
    name = serializers.CharField(min_length=2, max_length=30)
    class Meta:
        model = Room
        fields = ('id', 'home_id', 'name', 'pub_date', 'enable', 'type')
        read_only_fields = ('id', 'home_id', 'type')


class HomeDetailSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    owner_id = serializers.IntegerField(source='owner.id', read_only=True)
    rooms = RoomSerializer(many=True, read_only=True)

    class Meta:
        model = Home
        fields = ('id', 'owner_id', 'name', 'pub_date', 'enable', 'rooms')
        read_only_fields = ('id', 'owner_id')


class DeviceIndexSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    class Meta:
        model = Device
        fields = ('id', 'name', 'description')


class ChannelSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    class Meta:
        model = Channel
        fields = ('id', 'name', 'api_key', 'description')


class ElementSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    class Meta:
        model = Element
        fields = ('id', 'name', 'name_id', 'description')


class DeviceDetailSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    channels = ChannelSerializer(many=True, read_only=True)

    class Meta:
        model = Device
        fields = ('id', 'name', 'description', 'channels')


class ChannelDetailSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    elements = ElementSerializer(many=True, read_only=True)

    class Meta:
        model = Channel
        fields = ('id', 'name', 'api_key', 'description', 'elements')


class CommandIotSerializer(FriendlyErrorMessagesMixin, serializers.Serializer):
    command = serializers.CharField()

class RoomDevicesSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    device_id = serializers.IntegerField(source='device_id.id')
    room_id = serializers.IntegerField(source='room_id.id', read_only=True)
    device_name = serializers.CharField(min_length=2, max_length=30)
    channel = ChannelDetailSerializer(source='device_id', read_only=True)
    room_name = serializers.CharField(source='room_id.name', read_only=True)

    class Meta:
        model = RoomDevices
        fields = ('id', 'room_id', 'device_id','room_name', 'channel', 'device_name', 'pub_date', 'enable')
        read_only_fields = ('id', 'room_id')

class RoomDevicesCreateSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    mac_address = serializers.CharField(min_length=1, write_only=True)
    device_type_id = serializers.IntegerField(write_only=True)
    room_id = serializers.IntegerField(source='room_id.id', read_only=True)
    device_name = serializers.CharField(min_length=2, max_length=30)
    channel = ChannelDetailSerializer(source='device_id', read_only=True)
    room_name = serializers.CharField(source='room_id.name', read_only=True)
    device_id = serializers.IntegerField(source='device_id.id', read_only=True)

    def create(self, validated_data):
        validated_data.pop('device_type_id', None)
        validated_data.pop('mac_address', None)
        obj = RoomDevices.objects.create(**validated_data)
        return obj

    class Meta:
        model = RoomDevices
        fields = ('id', 'room_id', 'mac_address', 'device_type_id', 'device_id', 'room_name', 'channel', 'device_name', 'pub_date', 'enable')
        read_only_fields = ('id', 'room_id')
        write_only_fields = ['mac_address', 'device_type_id']

class RoomDevicesUpdateSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    room_id = serializers.IntegerField(source='room_id.id', read_only=True)
    device_name = serializers.CharField(min_length=2, max_length=30)
    channel = ChannelDetailSerializer(source='device_id', read_only=True)
    room_name = serializers.CharField(source='room_id.name', read_only=True)

    class Meta:
        model = RoomDevices
        fields = ('id', 'room_id', 'device_name', 'room_name', 'channel', 'pub_date', 'enable')
        read_only_fields = ('id', 'room_id')

class RoomDevicesMoveSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    room_id = serializers.IntegerField(source='room_id.id', read_only=True)
    channel = ChannelDetailSerializer(source='device_id', read_only=True)
    room_name = serializers.CharField(source='room_id.name', read_only=True)

    class Meta:
        model = RoomDevices
        fields = ('id', 'room_id', 'device_name', 'room_name', 'channel', 'pub_date', 'enable')
        read_only_fields = ('id', 'device_name')

class SceneSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    home_id = serializers.IntegerField(source='home.id', read_only=True)
    name = serializers.CharField(min_length=2, max_length=30)

    class Meta:
        model = Scene
        fields = ('id', 'home_id', 'name', 'pub_date', 'enable')
        read_only_fields = ('id', 'home_id')

class SceneDevicesSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    scene = serializers.IntegerField(source='scene.id', read_only=True)
    room_device_id = serializers.IntegerField(source='room_device_id.id')
    room_device = RoomDevicesSerializer(source='room_device_id', read_only=True)
    command = serializers.IntegerField()

    class Meta:
        model = SceneRoomDevices
        fields = ('id', 'scene', 'room_device_id', 'room_device', 'command', 'pub_date', 'enable')
        read_only_fields = ('id', 'scene')

class SceneDevicesUpdateSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    scene = serializers.IntegerField(source='scene.id', read_only=True)
    command = serializers.IntegerField()
    room_device = RoomDevicesSerializer(source='room_device_id', read_only=True)

    class Meta:
        model = SceneRoomDevices
        fields = ('id', 'scene', 'room_device_id', 'room_device', 'command', 'pub_date', 'enable')
        read_only_fields = ('id', 'scene')

class SceneDevicesCreateSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    def __init__(self, *args, **kwargs):
        many = kwargs.pop('many', True)
        super(SceneDevicesCreateSerializer, self).__init__(many=many, *args, **kwargs)
    scene = serializers.IntegerField(source='scene.id', read_only=True)
    room_device = RoomDevicesSerializer(source='room_device_id', read_only=True)

    class Meta:
        model = SceneRoomDevices
        fields = ('id', 'scene', 'room_device_id', 'command', 'room_device', 'pub_date', 'enable')

class ShareDeviceSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    shared_user = UserSerializer( read_only=True)
    origin_user = UserSerializer(read_only=True)
    shared_user_id = serializers.IntegerField(write_only=True)
    home_id = serializers.IntegerField(write_only=True)
    home = HomeSerializer(read_only=True)
    class Meta:
        model = ShareRoomDevices
        fields = ('id', 'origin_user', 'shared_user', 'shared_user_id', 'token', 'home_id', 'home', 'expired_at', 'pub_date', 'enable')
        read_only_fields=['pub_date', 'expired_at', 'token', 'enable']
