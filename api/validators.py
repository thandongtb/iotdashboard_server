from channels.models import Channel
from home_scenes.models import Scene
from home_rooms.models import Home
from home_rooms.models import Room
from scene_room_devices.models import SceneRoomDevices
from room_devices.models import RoomDevices
from rest_framework.response import Response
from devices.models import Device
from rest_framework.generics import get_object_or_404
from django.contrib.auth.models import User
from share_devices.models import ShareRoomDevices
from django.db.models import Q
from fcm_django.models import FCMDevice


class ApiResponseValidators(object):
    def validate_device_id(self, request):
        device = Channel.objects.filter(pk=request.data['device_id'])
        if not device:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'Device ID invalid.',
                'errors': [],
            })
        return True

    def validate_user_id(self, user_id):
        user = User.objects.filter(pk=user_id)
        if not user:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'User ID invalid.',
                'errors': [],
            })
        return True

    def validate_share_current_user(self, request):
        user = User.objects.filter(pk=request.data['shared_user_id'])
        if user.get().id == request.user.id:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'Share with same current user.',
                'errors': [],
            })
        return True

    def validate_scene_room_device(self, request, kwargs):
        scene_room_devices = SceneRoomDevices.objects.filter(scene_id=kwargs['scene_id_pk'], room_device_id=request.data['room_device_id'])
        if scene_room_devices:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'The room_device_id already exists in scene.',
                'errors': [],
            })
        return True

    def validate_share_object_id(self, request):
        share_object = Home.objects.filter(pk=request.data['home_id'], owner_id=request.user)

        if not share_object:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'The home_id invalid or access denied',
                'errors': [],
            })
        return True

    def validate_share_device(self, request):
        share_device = ShareRoomDevices.objects.filter(origin_user_id=request.user.id, shared_user_id=request.data['shared_user_id'], home_id=request.data['home_id'], enable=True)
        if share_device:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'The home already shared with this user',
                'errors': [],
            })
        return True

    def validate_room_device(self, request, kwargs):
        room_devices = RoomDevices.objects.filter(room_id=kwargs['room_id_pk'], device_id=request.data['device_id'])
        if room_devices:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'Device already exists in room.',
                'errors': [],
            })
        return True

    def validate_room_mac_address(self, request, kwargs):
        device = Device.objects.filter(pk=request.data['device_type_id']).get()
        channel = Channel.objects.filter(api_key=request.data['mac_address'], device=device,
                                    owner=request.user)
        if not channel:
            return True
        room_devices = RoomDevices.objects.filter(room_id=kwargs['room_id_pk'], device_id=channel.get().id)
        if room_devices:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'Device with mac_address {} already exists in room.'.format(request.data['mac_address']),
                'errors': [],
            })
        return True

    def validate_room_device_id(self, request):
        room_devices = RoomDevices.objects.filter(pk=request.data['room_device_id'])
        if not room_devices:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'The room_device_id invalid.',
                'errors': [],
            })
        return True

    def validate_multi_room_device_id(self, request):
        for d in request.data['devices']:
            scene_room_devices = RoomDevices.objects.filter(pk=d['room_device_id'])
            if not scene_room_devices:
                return Response({
                    'data': [],
                    'code': 4003.,
                    'message': 'The room_device_id {} invalid.'.format(d['room_device_id']),
                    'errors': [],
                })
        return True

    def validate_multi_room_device(self, request, kwargs):
        for d in request.data['devices']:
            scene_room_devices = SceneRoomDevices.objects.filter(scene_id=kwargs['scene_id_pk'], room_device_id=d['room_device_id'])
            if scene_room_devices:
                return Response({
                    'data': [],
                    'code': 4003.,
                    'message': 'Device {} already exists in scene.'.format(d['room_device_id']),
                    'errors': [],
                })
        return True

    def validate_room_device_name(self, request, kwargs):
        room_devices = RoomDevices.objects.filter(room_id=kwargs['room_id_pk'], device_name=request.data['device_name'])
        if room_devices:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'Device name is already exists in room.',
                'errors': [],
            })
        return True

    def validate_device_type_id(self, request, kwargs):
        device = Device.objects.filter(pk=request.data['device_type_id'])
        if not device:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'The device_type_id invalid',
                'errors': [],
            })
        return True

    def validate_scene(self, request, kwargs):
        scene = Scene.objects.filter(home_id=kwargs['home_id_pk'], pk=kwargs['scene_id_pk'])
        if not scene:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'Invalid scene or you don\'t have permission to access.',
                'errors': [],
            })
        return True

    def validate_scene_name(self, request, kwargs):
        scene = Scene.objects.filter(home_id=kwargs['home_id_pk'], name=request.data['name'])
        if scene:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'Scene name is already exists.',
                'errors': [],
            })
        return True

    def validate_home(self, request, kwargs):
        share_homes = ShareRoomDevices.objects.filter(shared_user=request.user, enable=True)
        share_home_id = [share_home.home.id for share_home in share_homes]
        if share_home_id:
            home = Home.objects.filter(Q(owner=request.user) | Q(pk__in=share_home_id) & Q(pk=kwargs['home_id_pk']))
        else:
            home = Home.objects.filter(owner=request.user, pk=kwargs['home_id_pk'])
        if not home:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'Invalid home or you don\'t have permission to access.',
                'errors': [],
            })
        return True

    def validate_share_home(self, request, kwargs):
        share_homes = ShareRoomDevices.objects.filter(Q(origin_user=request.user) | Q(shared_user=request.user) & Q(pk=kwargs['pk']))

        if not share_homes:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'Invalid share home or you don\'t have permission to access.',
                'errors': [],
            })
        return True

    def validate_home_name(self, request):
        home = Home.objects.filter(owner=request.user, name=request.data['name'])
        if home:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'Home name is already exists.',
                'errors': [],
            })
        return True

    def validate_room(self, kwargs):
        home = Room.objects.filter(pk=kwargs['room_id_pk'], home_id=kwargs['home_id_pk'])
        if not home:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'Invalid room or you don\'t have permission to access.',
                'errors': [],
            })
        return True

    def validate_room_name(self, request, kwargs):
        room = Room.objects.filter(home_id=kwargs['home_id_pk'], name=request.data['name'])
        if room:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'Room name is already exists in home.',
                'errors': [],
            })
        return True

    def validate_access_token(self, request, kwargs):
        devices = FCMDevice.objects.filter(registration_id=request.data['registration_id'])
        if devices:
            return Response({
                'data': [],
                'code': 4003.,
                'message': 'This token already exists.',
                'errors': [],
            })
        return True
