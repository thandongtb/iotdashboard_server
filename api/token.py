import hashlib
import struct

def get_hash_int(data):
    return hashlib.md5(struct.pack(">i", data)).hexdigest()
