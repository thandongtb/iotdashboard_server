from django.core.cache import cache
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from api.serializers import DeviceIndexSerializer, DeviceDetailSerializer
from .generic_views import ApiResource
from devices.models import Device

class IndexView(ApiResource, ListAPIView):
    queryset = Device.objects.all()
    serializer_class = DeviceIndexSerializer
    pagination_class = PageNumberPagination

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

class DetailView(ApiResource, RetrieveAPIView):
    queryset = Device.objects.prefetch_related('channels').all()
    serializer_class = DeviceDetailSerializer
    lookup_url_kwarg = 'device_id'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)