from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from api.serializers import ChannelDetailSerializer
from channels.models import Channel

class DetailView(
    RetrieveAPIView):
    queryset = Channel.objects.prefetch_related('elements').all()
    serializer_class = ChannelDetailSerializer
    lookup_url_kwarg = 'channel_api_key'
    lookup_field = 'api_key'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response({
            'code' : 1,
            'message' : 'Get channel detail successful',
            'errors' : [],
            'data' : serializer.data
        })