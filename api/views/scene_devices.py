from rest_framework import permissions
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from api.serializers import SceneDevicesSerializer, SceneDevicesUpdateSerializer, SceneDevicesCreateSerializer
from channels.models import Channel
from home_scenes.models import Scene
from scene_room_devices.models import SceneRoomDevices
from rest_framework.response import Response
from api.validators import ApiResponseValidators
from room_devices.models import RoomDevices

class SceneDevicesView(ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    validators = ApiResponseValidators()

    def get_serializer_class(self):
        if self.action in ('update', 'partial_update'):
            return SceneDevicesUpdateSerializer
        elif self.action in ('create'):
            return SceneDevicesCreateSerializer
        else:
            return SceneDevicesSerializer

    def get_queryset(self):
        return SceneRoomDevices.objects.filter(scene_id=self.kwargs['scene_id_pk'])

    def perform_create_multiple(self, serializer):
        scene = get_object_or_404(Scene.objects.all(), pk=self.kwargs['scene_id_pk'])

        serializer.save(
            scene=scene
        )

    def perform_create(self, serializer):
        scene = get_object_or_404(Scene.objects.all(), pk=self.kwargs['scene_id_pk'])
        room_device = get_object_or_404(RoomDevices.objects.all(), pk=self.request.data['room_device_id'])

        serializer.save(
            scene=scene,
            room_device_id=room_device
        )

    def perform_update(self, serializer):
        scene = get_object_or_404(Scene.objects.all(), pk=self.kwargs['scene_id_pk'])
        serializer.save(
            scene=scene,
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        validate = self.validators.validate_scene(request, kwargs=self.kwargs)
        if validate != True:
            return validate

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'List devices in scene successfully',
            'errors': [],
        })

    def create(self, request, *args, **kwargs):
        validate = self.validators.validate_scene(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        if 'devices' in request.data:
            validate = self.validators.validate_multi_room_device_id(request)
            if validate != True:
                return validate
            validate = self.validators.validate_multi_room_device(request, kwargs=self.kwargs)
            if validate != True:
                return validate

            serializer = self.get_serializer(data=request.data['devices'], many=True)
            serializer.is_valid(raise_exception=True)
            self.perform_create_multiple(serializer)
        else:
            validate = self.validators.validate_room_device_id(request)
            if validate != True:
                return validate
            validate = self.validators.validate_scene_room_device(request, kwargs=self.kwargs)
            if validate != True:
                return validate

            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Create room device in scene successfully',
            'errors': [],
        }, status=status.HTTP_201_CREATED, headers=headers)


    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)

        validate = self.validators.validate_scene(request, kwargs=self.kwargs)
        if validate != True:
            return validate

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Get detail device in scene successfully',
            'errors': [],
        })


    def update(self, request, *args, **kwargs):
        validate = self.validators.validate_scene(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        if 'room_device_id' in request.data:
            validate = self.validators.validate_room_device_id(request)
            if validate != True:
                return validate
            validate = self.validators.validate_scene_room_device(request, kwargs=self.kwargs)
            if validate != True:
                return validate

        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Update device in scene successfully',
            'errors': [],
        })


    def destroy(self, request, *args, **kwargs):
        validate = self.validators.validate_scene(request, kwargs=self.kwargs)
        if validate != True:
            return validate

        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({
            'data': [],
            'code': 1,
            'message': 'Delete device in scene successfully',
            'errors': [],
        })