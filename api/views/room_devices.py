from rest_framework import permissions
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from api.serializers import RoomDevicesSerializer, RoomDevicesUpdateSerializer, RoomDevicesCreateSerializer
from channels.models import Channel
from home_rooms.models import Home
from home_rooms.models import Room
from room_devices.models import RoomDevices
from rest_framework.response import Response
from api.validators import ApiResponseValidators
from devices.models import Device
from django.core import serializers

class RoomDevicesView(ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    validators = ApiResponseValidators()

    def get_serializer_class(self):
        if self.action in ('update', 'partial_update'):
            return RoomDevicesUpdateSerializer
        elif self.action in ('create'):
            return RoomDevicesCreateSerializer
        else:
            return RoomDevicesSerializer

    def get_queryset(self):
        return RoomDevices.objects.filter(room_id_id=self.kwargs['room_id_pk'])

    def perform_create(self, serializer):
        room = get_object_or_404(Room.objects.all(), pk=self.kwargs['room_id_pk'])
        device = Device.objects.filter(pk=self.request.data['device_type_id']).get()
        channel = Channel.objects.filter(api_key=self.request.data['mac_address'], device=device,
                                         owner=self.request.user)
        if not channel:
            channel = Channel.objects.create(api_key=self.request.data['mac_address'], device=device, owner=self.request.user)
        else:
            channel = channel.get()
        serializer.save(
            room_id=room,
            device_id=channel
        )

    def perform_update(self, serializer):
        room = get_object_or_404(Room.objects.all(), pk=self.kwargs['room_id_pk'])
        serializer.save(
            room_id=room,
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        validate = self.validators.validate_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        validate = self.validators.validate_room(self.kwargs)
        if validate != True:
            return validate

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'List devices in room successfully',
            'errors': [],
        })

    def create(self, request, *args, **kwargs):
        validate = self.validators.validate_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        validate = self.validators.validate_room(self.kwargs)
        if validate != True:
            return validate
        validate = self.validators.validate_device_type_id(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        validate = self.validators.validate_room_mac_address(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        validate = self.validators.validate_room_device_name(request, kwargs=self.kwargs)
        if validate != True:
            return validate

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        
        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Create device in room successfully',
            'errors': [],
        }, status=status.HTTP_201_CREATED)


    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)

        validate = self.validators.validate_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        validate = self.validators.validate_room(self.kwargs)
        if validate != True:
            return validate

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Get detail device in room successfully',
            'errors': [],
        })


    def update(self, request, *args, **kwargs):
        validate = self.validators.validate_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        validate = self.validators.validate_room(self.kwargs)
        if validate != True:
            return validate
        validate = self.validators.validate_room_device_name(request, kwargs=self.kwargs)
        if validate != True:
            return validate

        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Update device in room successfully',
            'errors': [],
        })


    def destroy(self, request, *args, **kwargs):
        validate = self.validators.validate_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        validate = self.validators.validate_room(self.kwargs)
        if validate != True:
            return validate

        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({
            'data': [],
            'code': 1,
            'message': 'Delete device in room successfully',
            'errors': [],
        })