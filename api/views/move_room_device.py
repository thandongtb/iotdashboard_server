from rest_framework.generics import GenericAPIView
from rest_framework.mixins import UpdateModelMixin
from room_devices.models import RoomDevices
from api.serializers import RoomDevicesMoveSerializer
from api.validators import ApiResponseValidators
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from home_rooms.models import Room

class MoveRoomDeviceView(GenericAPIView, UpdateModelMixin):
    permission_classes = (permissions.IsAuthenticated,)
    validators = ApiResponseValidators()

    queryset = RoomDevices.objects.all()
    serializer_class = RoomDevicesMoveSerializer

    def perform_update(self, serializer):
        room = get_object_or_404(Room.objects.all(), pk=self.kwargs['room_id_pk'])
        serializer.save(
            room_id=room,
        )

    def patch(self, request, *args, **kwargs):
        validate = self.validators.validate_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        validate = self.validators.validate_room(self.kwargs)
        if validate != True:
            return validate
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Move device to new room successfully',
            'errors': [],
        })