from django.http import JsonResponse
from command_iot.command_recognition import CommandRecognition
from api.serializers import CommandIotSerializer
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_registration.decorators import api_view_serializer_class_getter

@api_view_serializer_class_getter(
    lambda: CommandIotSerializer)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def detect_command(request):
    serializer = CommandIotSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    data = serializer.validated_data

    try:
        recognizer = CommandRecognition(sentence=data['command'])
        data = recognizer.detect_iot_commands()
    except:
        return JsonResponse({
            'code': 4000,
            'message': 'The command detection failed.',
            'data': [],
            'errors': []
        }, status=400)
    return JsonResponse({
        'code': 1,
        'message': 'The command detection successful',
        'data': data,
        'errors' : []
    }, safe=False)
