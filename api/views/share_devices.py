from rest_framework.generics import GenericAPIView
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin
from room_devices.models import RoomDevices
from api.serializers import ShareDeviceSerializer
from api.validators import ApiResponseValidators
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ModelViewSet
from user_homes.models import Home
from django.contrib.auth.models import User
from share_devices.models import ShareRoomDevices
from api.token import get_hash_int
from time import time
from datetime import timedelta
from django.utils import timezone
from django.db.models import Q
from push_notification import push_notification

class ShareDeviceConfirmView(GenericAPIView, RetrieveModelMixin):
    validators = ApiResponseValidators()
    queryset = ShareRoomDevices.objects.all()
    serializer_class = ShareDeviceSerializer

    def perform_update(self, serializer):
        serializer.save(
            enable=True
        )

    def get(self, request, *args, **kwargs):
        share_device = ShareRoomDevices.objects.filter(Q(token__exact=self.kwargs['token']) & Q(expired_at__gte=timezone.now()) & Q(enable=False))
        if (share_device):
            serializer = self.get_serializer(share_device.get(), data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response({
                'data': serializer.data,
                'code': 1,
                'message': 'Confirm share home successfully',
                'errors': [],
            })
        return Response({
            'data': [],
            'code': 4003,
            'message': 'Token invalid or expired',
            'errors': [],
        })

class ShareDevicePendingView(GenericAPIView, RetrieveModelMixin):
    validators = ApiResponseValidators()
    queryset = ShareRoomDevices.objects.all()
    serializer_class = ShareDeviceSerializer

    def get(self, request, *args, **kwargs):
        share_device = ShareRoomDevices.objects.filter(Q(shared_user=self.request.user) & Q(enable=False))
        serializer = self.get_serializer(share_device, many=True)

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'List pendding share request successfully',
            'errors': [],
        })

class ShareDeviceView(ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    validators = ApiResponseValidators()

    serializer_class = ShareDeviceSerializer

    def get_queryset(self):
        return ShareRoomDevices.objects.filter(origin_user=self.request.user)

    def perform_create(self, serializer):
        home = get_object_or_404(Home.objects.all(), pk=self.request.data['home_id'])
        shared_user = get_object_or_404(User.objects.all(), pk=self.request.data['shared_user_id'])

        serializer.save(
            home=home,
            origin_user = self.request.user,
            shared_user = shared_user,
            token=get_hash_int(self.request.user.id) + get_hash_int(int(time())),
            enable=False
        )

    def perform_reshare(self, serializer):
        serializer.save(
            token=get_hash_int(self.request.user.id) + get_hash_int(int(time())),
            pub_date = timezone.now(),
            expired_at = timezone.now()+timedelta(days=7)
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'List shared homes successfully',
            'errors': [],
        })

    def create(self, request, *args, **kwargs):
        validate = self.validators.validate_user_id(self.request.data['shared_user_id'])
        if validate != True:
            return validate
        validate = self.validators.validate_share_current_user(self.request)
        if validate != True:
            return validate
        validate = self.validators.validate_share_object_id(self.request)
        if validate != True:
            return validate
        validate = self.validators.validate_share_device(self.request)
        if validate != True:
            return validate
        share_device = ShareRoomDevices.objects.filter(origin_user_id=request.user.id,
                                                           shared_user_id=request.data['shared_user_id'],
                                                           home_id=request.data['home_id'])
        title = "IK-Home"
        body = "Bạn nhận được yêu cầu chia sẻ nhà từ {}. Vui lòng xác nhận yêu cầu trong vòng 7 ngày.".format(
            request.user.username)

        if share_device:
            serializer = self.get_serializer(share_device.get(), data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            self.perform_reshare(serializer)
            notify_data = {
                "token" : serializer.data['token']
            }

            shared_user = get_object_or_404(User.objects.all(), pk=serializer.data['shared_user']['id'])
            push_notification.push_share_notification(user=shared_user, title=title, body=body, data=notify_data)
            return Response({
                'data': serializer.data,
                'code': 1,
                'message': 'Re-share home successfully',
                'errors': [],
            })

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        notify_data = {
            "token": serializer.data['token']
        }
        shared_user = get_object_or_404(User.objects.all(), pk=serializer.data['shared_user']['id'])
        push_notification.push_share_notification(user=shared_user, title=title, body=body, data=notify_data)

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Share home successfully',
            'errors': [],
        })

    def retrieve(self, request, *args, **kwargs):
        validate = self.validators.validate_share_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate

        instance = self.get_object()
        serializer = self.get_serializer(instance)

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Get detail device in room successfully',
            'errors': [],
        })

    def update(self, request, *args, **kwargs):
        validate = self.validators.validate_share_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate

        partial = kwargs.pop('partial', False)

        if self.request.data['shared_user_id']:
            validate = self.validators.validate_user_id(self.request.data['shared_user_id'])
            if validate != True:
                return validate
            validate = self.validators.validate_share_current_user(self.request)
            if validate != True:
                return validate

        if self.request.data['home_id']:
            validate = self.validators.validate_share_object_id(self.request)
            if validate != True:
                return validate

        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Update share home successfully',
            'errors': [],
        })


    def destroy(self, request, *args, **kwargs):
        validate = self.validators.validate_share_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate

        instance = ShareRoomDevices.objects.filter(Q(origin_user=request.user) | Q(shared_user=request.user) & Q(pk=kwargs['pk']))
        self.perform_destroy(instance)
        return Response({
            'data': [],
            'code': 1,
            'message': 'Delete share home successfully',
            'errors': [],
        })
