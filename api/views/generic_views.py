from oauth2_provider.views.generic import ScopedProtectedResourceView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt


@method_decorator(csrf_exempt, name='dispatch')
class ApiResource(ScopedProtectedResourceView):
    required_scopes = []
