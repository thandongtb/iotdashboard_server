from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet
from api.serializers import SceneSerializer
from home_scenes.models import Scene
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import get_object_or_404
from user_homes.models import Home
from api.validators import ApiResponseValidators

class SceneView(ModelViewSet):
    serializer_class = SceneSerializer
    permission_classes = (permissions.IsAuthenticated,)
    validators = ApiResponseValidators()

    def get_queryset(self):
        return Scene.objects.filter(home_id_id=self.kwargs['home_id_pk'])

    def perform_create(self, serializer):
        home = get_object_or_404(Home.objects.all(), pk=self.kwargs['home_id_pk'])
        serializer.save(
            home_id=home
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        validate = self.validators.validate_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'List scene successfully',
            'errors': [],
        })

    def create(self, request, *args, **kwargs):
        validate = self.validators.validate_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        validate = self.validators.validate_scene_name(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Create Scene successfully',
            'errors': [],
        }, status=status.HTTP_201_CREATED, headers=headers)

    def retrieve(self, request, *args, **kwargs):
        validate = self.validators.validate_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Get detail Scene successfully',
            'errors': [],
        })

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        validate = self.validators.validate_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        validate = self.validators.validate_scene_name(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Update Scene successfully',
            'errors': [],
        })

    def destroy(self, request, *args, **kwargs):
        validate = self.validators.validate_home(request, kwargs=self.kwargs)
        if validate != True:
            return validate
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({
            'data': [],
            'code': 1,
            'message': 'Delete Scene successfully',
            'errors': [],
        })