from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet
from api.serializers import HomeSerializer
from user_homes.models import Home
from rest_framework.response import Response
from rest_framework import status
from api.validators import ApiResponseValidators
from api.permissions import IsOwner
from django.db.models import Q
from share_devices.models import ShareRoomDevices

class HomeView(ModelViewSet):
    serializer_class = HomeSerializer
    permission_classes = (permissions.IsAuthenticated,)
    validators = ApiResponseValidators()

    def get_queryset(self):
        share_homes = ShareRoomDevices.objects.filter(shared_user=self.request.user, enable=True)
        share_home_id = [share_home.home.id for share_home in share_homes]
        if share_homes:
            return Home.objects.filter(Q(owner=self.request.user) | Q(pk__in=share_home_id))
        return Home.objects.filter(Q(owner=self.request.user))

    def perform_create(self, serializer):
        serializer.save(
            owner=self.request.user
        )

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'List home successfully',
            'errors': [],
        })

    def create(self, request, *args, **kwargs):
        validate = self.validators.validate_home_name(request)
        if validate != True:
            return validate
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Create home successfully',
            'errors': [],
        }, status=status.HTTP_201_CREATED, headers=headers)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Get detail home successfully',
            'errors': [],
        })

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        if 'name' in request.data:
            validate = self.validators.validate_home_name(request)
            if validate != True:
                return validate
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response({
            'data': serializer.data,
            'code': 1,
            'message': 'Update home successfully',
            'errors': [],
        })

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response({
            'data': [],
            'code': 1,
            'message': 'Delete home successfully',
            'errors': [],
        })