from django.conf import settings
from django.conf.urls.static import static
from rest_framework_nested import routers
from api.views import homes
from api.views import scenes
from api.views import rooms
from api.views import room_devices
from api.views import scene_devices
from api.views import share_devices

router = routers.SimpleRouter()
router.register(r'home', homes.HomeView, base_name='home')
router.register(r'share', share_devices.ShareDeviceView, base_name='share')

home_router = routers.NestedSimpleRouter(router, r'home', lookup='home_id')
home_router.register(r'room', rooms.RoomView, base_name='room')
home_router.register(r'scene', scenes.SceneView, base_name='scene')

scene_router = routers.NestedSimpleRouter(home_router, r'scene', lookup='scene_id')
scene_router.register(r'scene_devices', scene_devices.SceneDevicesView, base_name='scene_devices')

room_router = routers.NestedSimpleRouter(home_router, r'room', lookup='room_id')
room_router.register(r'room_devices', room_devices.RoomDevicesView, base_name='room_device')

urlpatterns = router.urls + home_router.urls + room_router.urls + scene_router.urls
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
