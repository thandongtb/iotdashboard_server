# -*- coding: utf-8 -*-
"""
Room device manager

"""

from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from channels.models import Channel
from home_rooms.models import Room
from django.utils.translation import ugettext_lazy as _

@python_2_unicode_compatible
class RoomDevices(models.Model):
    """
    Room device manager.
    Management the devices in the rooms they are using
    The device here equivalent with a channel in model.
    """
    room_id          = models.ForeignKey(Room, on_delete=models.CASCADE, db_index=True, related_name='channels')
    device_id          = models.ForeignKey(Channel, on_delete=models.CASCADE, db_index=True, related_name='channels')
    device_name           = models.CharField(_('room_device_name'), max_length=30, help_text="Device name created by user")
    pub_date       = models.DateTimeField(_('pub_date'), auto_now=True)
    enable         = models.BooleanField(_('enable'), default=True)

    class Meta:
        unique_together = (('room_id', 'device_id'),('room_id', 'device_name'))

    def __str__(self):
        """
        """
        return self.device_name
