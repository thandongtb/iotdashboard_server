from django.contrib import admin

# Register your models here.
from .models import RoomDevices

class RoomDevicesAdmin(admin.ModelAdmin):
    list_display = ('room_id', 'device_id','device_name', 'enable')

admin.site.register(RoomDevices, RoomDevicesAdmin)