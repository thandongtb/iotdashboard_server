from django.apps import AppConfig


class UserDeviceConfig(AppConfig):
    name = 'room_devices'
