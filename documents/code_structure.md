# Cấu trúc thư mục 

## Thư mục chính /iotdashboard/ 

![](http://sv1.upsieutoc.com/2018/06/19/Screenshot-from-2018-06-19-13-25-35.png)

Thư mục này bao gồm các file cấu hình chính của dự án trong đó bao gồm 

### File /iotdashboard/settings.py 

File này chứa các cài đặt chính của hệ thống. Mỗi khi muốn thêm một module mới nào hay cần config gì thì cài đặt trong file này.

Trong ``settings.py`` có một số config quan trọng. Đầu tiên là *INSTALLED_APPS* 
```bash
INSTALLED_APPS = [
    'api',
    'user_homes',
    'home_scenes',
    'scene_room_devices',
    'home_rooms',
    'landing_page.apps.LandingPageConfig',
    'social_django',
    'share_devices',
    'django_filters',
    'user_otp',
]
```

sử dụng để config các ứng dụng sử dụng trong project. Mỗi module được tạo ra sẽ ở trong thư mục này.

### File /iotdashboard/handlers.py 

File này sử dụng để định dạng format của response trả về trong API giống như format dạng 

```bash
{
    "code" : 1234,
    "message" : "Message",
    "data" : [],
    "errors" : []
}
```

### File /iotdashboard/serializers.py 

File này sử dụng để tạo định format thông báo khi xảy ra lỗi validate trong serializers. Tìm hiểu về serializers tại [đây](http://www.django-rest-framework.org/api-guide/serializers/)

### File /iotdashboard/mqtt.py

File này sử đụng dể khởi chạy mqtt client lắng nghe dữ liệu của hệ thống rồi lưu vào InfluxDB trong bảng *smart_seed_button*

### File /iotdashboard/urls.py

File này được sử dụng để config tất cả các urls của hệ thống. Bao gồm cả phần urls của web và api 


  
## Thư mục /api/ 
Thư mục này đóng vai trò như một module điều hướng các api thông qua phương thức RESTFul API 

![](https://sv1.uphinhnhanh.com/images/2018/06/19/Screenshotfrom2018-06-1913-40-43.png)

Thư mục này chứa toàn bộ code của phần api trong đó quan trọng nhất của các phần sau:

### File serializers.py 

File này chứa toàn bộ các serializers sử dụng cho các API. Có thể hiểu cơ bản serializers chính là thứ để quy định chúng ta sẽ nhập những trường gì trong API. Từ serializers này sẽ được hiển thị trên trang documents các trường cần nhập vào.

Ví dụ một serializers về thêm Room vào nhà

```bash
class RoomSerializer(FriendlyErrorMessagesMixin, ModelSerializer):
    home_id = serializers.IntegerField(source='home.id', read_only=True)
    name = serializers.CharField(min_length=2, max_length=30)
    class Meta:
        model = Room
        fields = ('id', 'home_id', 'name', 'pub_date', 'enable', 'type')
        read_only_fields = ('id', 'home_id', 'type')
```

Ở đây có hai trường là home_id và name cần thêm vào từ API. 

Kết quả hiển thị trên trang docs như sau:

![](https://sv1.uphinhnhanh.com/images/2018/06/19/Screenshotfrom2018-06-1913-50-08.png) 

Còn dòng fields là để liệt kê những fields nào cần hiển thị ra trên API. Ví dụ 

```bash
fields = ('id', 'home_id', 'name', 'pub_date', 'enable', 'type')
```

thì khi vào API list room hay room detail sẽ hiển thị các trường như vậy 

![](https://sv1.uphinhnhanh.com/images/2018/06/19/Screenshotfrom2018-06-1913-53-52.png)

Tất cả các serializers khác đều có cấu trúc tương tự 

### Thư mục /api/views/

Đây là thư mục quan trọng nhất của Project. Đóng vai trò như Controller trong mô hình MVC. Nó la nơi trung tâm xử lý các yêu cầu từ load request đến xử lý request, gọi serializers tương ứng 

![](https://sv1.uphinhnhanh.com/images/2018/06/19/Screenshotfrom2018-06-1913-55-20.png)

