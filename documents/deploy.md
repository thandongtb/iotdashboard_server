# Yêu cầu hệ thống 
Thực hiện đầy đủ các bước cấu hình trên server bằng file ``install.md`` 
## Khởi chạy server sử dụng gunicorn 

```bash
ssh root@128.199.214.241

source ~/iot_server/bin/activate

cd ~/code/iotdashboard_server/

pkill gunicorn

gunicorn iotdashboard.wsgi -k gevent --daemon
```
