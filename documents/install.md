# Hướng dẫn cài đặt môi trường phát triển 

## Cài đặt môi trường Django 

### Step 1: Cài đặt package cần thiết theo link [sau](https://gist.github.com/Geoyi/d9fab4f609e9f75941946be45000632b) 

### Step 2: Tạo virtualenv folder 

```bash
virtualenv -p python3 ~/iot_server
```

### Step 3: Truy cập virtualenv 

```bash
source ~/iot_server/bin/activate
```

### Step 4: Clone code github 

```bash
mkdir ~/code 
cd ~/code 
git clone git@bitbucket.org:thandongtb/iotdashboard_server.git
cd iotdashboard_server
```

### Step 5: Cài đặt các package cần thiết 

```bash
pip install -r requirements.txt
```

### Step 6: Copy file môi trường .env và config 

```bash
cp .env-example .env
```
Sau đó cài đặt lại các thông số cho cần thiết trong file .env. Ví dụ:

```bash
APP_KEY=mwtdcUe4uOvvJjDk2AuQ9Mq2xiHPw3740m5iGLf6hwg3B4TNSx
APP_DEBUG=True
APP_URL=http://localhost:8001

DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
DB_HOST=
DB_PORT=

MEDIA_ROOT=storage
MEDIA_URL=/media/

STATIC_URL=/static/
STATIC_ROOT=static
```

Phần cấu hình DB 

```bash
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
DB_HOST=
DB_PORT=
```
Được áp dụng cho trường hợp sử dụng DB là Postgres hoặc MySQL. Nếu sử dụng SQLite thì chưa cần điền thông tin này. 

### Cấu hình database 

Sau khi config xong file .env cần cấu hình lại DB để test. DB đang được sử dụng mặc định là SQLite có thể thấy trong file cấu hình ``iotdashboard/settings.p`` 

```bash
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'iotdashboard_db.sqlite3'),
    }
}
```

Tạo file rỗng  ``iotdashboard_db.sqlite3`` trong thư mục gốc. 

Nếu sử dụng Postgres thì bỏ comment dòng này 

```bash
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': env('DB_DATABASE'),
#         'USER': env('DB_USERNAME'),
#         'PASSWORD': env('DB_PASSWORD'),
#         'HOST': env('DB_HOST'),
#         'PORT': env('DB_PORT'),
#     }
# }
```

### Tạo migrations 

Trong **mỗi lần thay đổi model** cần chạy lại migrations 

```bash
python manage.py makemigrations

python manage.py migrate --run-syncdb
```

## Install MQTT Broker

Hiện tại đang sử dụng mosquitto MQTT Broker. Hướng dẫn cài đặt chi tiết cho Ubuntu 16.04 tại [đây](https://www.vultr.com/docs/how-to-install-mosquitto-mqtt-broker-server-on-ubuntu-16-04) 

Có một vài câu lệnh quan trọng 

```bash
INSTALL MOSQUITTO

sudo apt-get install mosquitto mosquitto-clients -y

SET PASSWORD

sudo mosquitto_passwd -c /etc/mosquitto/passwd <user_name>

sudo gedit /etc/mosquitto/mosquitto.conf

ADD TO CONFIG FILE

password_file /etc/mosquitto/passwd

allow_anonymous false

START BROKER

mosquitto -c /etc/mosquitto/mosquitto.conf

RESTART

sudo systemctl restart mosquitto

TEST WITH PASS

mosquitto_sub -h localhost -t test -u "ppower" -P "ppower"

mosquitto_pub -h localhost -t test -m "api_key_1_on" -u "ppower" -P "ppower"

PPOWERTECH SERVER

mosquitto_sub -h ppowertech.vn -t test -u "ppowertech2018" -P "ppowertech2018"

mosquitto_pub -h ppowertech.vn -t test -m "api_key_1_on" -u "ppowertech2018" -P "ppowertech2018"

username/password: ppowertech2018
```

### Cài đặt InfluxDB và Grafana để lưu trữ Bigdata 

INSTALL INFLUXDB

Cài đặt theo hướng dẫn tại [đây](https://larsbergqvist.wordpress.com/2017/03/02/influxdb-and-grafana-for-sensor-time-series/
)
INSTALL GRAFANA
Cài đặt Grafana theo hướng dẫn tại [đây](http://docs.grafana.org/installation/debian/)

Khởi động service 
```bash
sudo service grafana-server start
```

### Cài đặt Nginx 

Cài đặt Nginx theo hướng dẫn tại [đây](https://www.rosehosting.com/blog/how-to-install-nginx-on-ubuntu-16-04/)

Copy file cài đặt trong thư mục `nginx/example.conf` vào thư mục cấu hình nginx 

```bash
cp nginx/example.conf /etc/nginx/sites-enabled/
```

Khởi động lại cấu hình nginx 

```bash
sudo systemctl restart nginx
```

### Cấu hình file hosts cho server 

Thêm thông tin ``servername`` được cấu hình trong file nginx vào file hosts 

```bash
127.0.0.1 ppowertech.vn
```

# Khởi chạy dự án 

```bash
source ~/iot_server/bin/activate

cd ~/code/iotdashboard_server/

python manage.py runserver
```